export default class Hero{
	/**
	 * @param {string} name
	 * @param {Number} health
	 */
	constructor(name, health){
			this._name      = name;
			this._health    = health;
			this._totalHealth = health;
			this._dammage   = 30;
			this._defence   = 2;
			this._lastPunch = 0;
			this.sprite 		= null;
			this.soundPunch = null;
			this.state			=  "idle" // attack, dead
	}
	
	set name(value){
			this._name = value;
	}

	get name() {
			return this._name;
	}
	set health(value) {
			if( value < 0 ){
					this._health = 0;
					this.state = "dead";
					console.warn(this.name, ' est mort');
			}else{
					this._health = value;
			}
		
	}
	get health() {
			return this._health;
	}
	get dammage(){
			return this._dammage;
	}
	get defence(){
			return this._defence;
	}
	get totalHealth(){
		return this._totalHealth;
	}
	get percentHealth(){
		return (this.health / this.totalHealth) * 100;
	}
	getFromApi(){
		// let url = 'https://www.superheroapi.com/api.php/10156029975936156/10';
	}
	takeDammage(dammage){
			let audio = new Audio('/assets/mp3/ouch.mp3');
			setTimeout(()=>{
				audio.play();
			}, 100);
			console.log(dammage, this.health )
			this.health = (this.health - dammage) ;
			if(this.health == 0){
					console.warn(this.name,' est mort');
			}
	}

	punch(target){
		this.state = 'punch';
		let audio = new Audio('/assets/mp3/slap.mp3');
		audio.play();
		setTimeout(()=>{
			target.takeDammage( this.dammage - target.defence );
			this.state = 'idle';
		},200)
	}
}
